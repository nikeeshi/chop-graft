package main;

import types.*;

public class Main {
	public int depth=0;
	public void begin(String s){
		for(int i=0;i<depth;i++)System.out.print("  ");
		System.out.println(s+" begin");
		depth++;
	}	
	public void end(String s){
		depth--;
		for(int i=0;i<depth;i++)System.out.print("  ");
		System.out.println(s+" end");
	}
	public void log(String s){
		for(int i=0;i<depth;i++)System.out.print("  ");
		System.out.println(s+" log");
	}
	public static void main(String[] args) {
		new Main().run(new TCarg());
	}
	public void run(TCarg arg){
		begin("run");
		chopPoint(arg);
		end("run");
	}
	public TC chopPoint(TCarg x) {
		begin("chopPoint");
		try{
		advicePoint(new TAarg());
		advicePoint(new TAarg());
		}finally{
			end("chopPoint finally");
		}
		log("chopPoint end");
		return null;
	}

	public TA advicePoint(TAarg x) {
		begin("advicePoint");
		end("advicePoint");
		return null;
	}
}
