package lib;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import main.Main;
import types.TA;
import types.TAarg;
import types.TC;
import types.TCarg;
import exceptions.AlreadyClosed;
import exceptions.NoGraftExecution;
import exceptions.RunWithoutExecution;

public aspect ChopGraft {
	public class ContinueWithValueMessage implements ContinueMessage {
		TC value;

		public ContinueWithValueMessage(TC value) {
			this.value = value;
		}
	}

	public class ContinueWithThrowMessage implements ContinueMessage {
	}

	public interface ContinueMessage {
	}

	public class ContinuePoint {
		BlockingQueue<ContinueMessage> queue;
		boolean endflag = false;

		ContinuePoint() {
			queue = new LinkedBlockingQueue<ContinueMessage>(1);
		}

		public void put(ContinueMessage e) throws InterruptedException,
				AlreadyClosed {
			if (endflag)
				throw new AlreadyClosed();
			queue.put(e);
		}

		public ContinueMessage take() throws InterruptedException {
			return queue.take();
		}

		public void close() {
			endflag = true;
		}
	}

	public abstract class Advice extends Thread {
		ResultReceivePoint refOfResultPoint;
		public ContinuePoint continuePoint = new ContinuePoint();

		public abstract TC advice() throws RunWithoutExecution;

		Advice(ResultReceivePoint refOfResultPoint) {
			this.refOfResultPoint = refOfResultPoint;
		}

		// return to refOfResultPoint
		public void run() {
			try {
				TC ret = null;
				try {
					ret = advice();
					continuePoint.close();
					refOfResultPoint.put(new ReturnMessage(ret));
				} catch (RunWithoutExecution e) {
					continuePoint.close();
					refOfResultPoint.put(new ExceptionThrownMessage());
				}
			} catch (InterruptedException e1) {// 無視
			}
		}

		public TC graft(TA ret) throws RunWithoutExecution {
			try {
				refOfResultPoint.put(new GraftCalledMessage(ret));
				ContinueMessage mes = continuePoint.take();
				if (mes instanceof ContinueWithValueMessage) {
					return ((ContinueWithValueMessage) mes).value;
				} else if (mes instanceof ContinueWithThrowMessage) {
					throw new RunWithoutExecution();
				}
			} catch (InterruptedException e) {// 無視
			}
			// unreachable
			return null;
		}
	}

	public class ExceptionThrownMessage implements ResultMessage {
		public ExceptionThrownMessage() {
		}
	}

	public class GraftCalledMessage implements ResultMessage {
		TA graftedvalue;

		public GraftCalledMessage(TA ret) {
			this.graftedvalue = ret;
		}
	}

	public class PairOfContinueResultPoints {
		public PairOfContinueResultPoints(ContinuePoint continuePoint,
				ResultReceivePoint receivePoint) {
			this.continuePoint = continuePoint;
			this.receivePoint = receivePoint;
		}

		ContinuePoint continuePoint;
		ResultReceivePoint receivePoint;
	}

	public interface ResultMessage {
	}

	public class ResultReceivePoint extends LinkedBlockingQueue<ResultMessage>
			implements BlockingQueue<ResultMessage> {
		private static final long serialVersionUID = 1L;

		ResultReceivePoint() {
			super(1);
		}
	}

	public class ReturnMessage implements ResultMessage {
		TC returnvalue;

		public ReturnMessage(TC returnvalue) {
			this.returnvalue = returnvalue;
		}
	}

	public static Stack<Queue<PairOfContinueResultPoints>> stack = new Stack<Queue<PairOfContinueResultPoints>>();

	public static class StackQueue {

		public static Queue<PairOfContinueResultPoints> queue() {
			return stack.peek();
		}

		public static void push() {
			stack.push(new LinkedList<PairOfContinueResultPoints>());
		}

		public static void pop() {
			stack.pop();
		}

	}

	pointcut chopPoint(TCarg x):
		call(TC Main.chopPoint(TCarg))&&args(x);

	pointcut advicePoint(TAarg x):
		call(TA Main.advicePoint(TAarg))&&args(x);

	pointcut advicePointPCD(TAarg x):
		advicePoint(x)&&cflowbelow(chopPoint(*));

	TC around(TCarg args):chopPoint(args){
		TC ret;
		StackQueue.push();
		try {
			ret = proceed(args);// proceed(args);
			while (!StackQueue.queue().isEmpty()) {
				PairOfContinueResultPoints pair = StackQueue.queue().poll();
				try {
					try {
						pair.continuePoint
								.put(new ContinueWithValueMessage(ret));
					} catch (InterruptedException e) {// 無視
					}
				} catch (AlreadyClosed e) {
				}// ありえない
				ResultMessage returnmessage = null;
				try {
					returnmessage = pair.receivePoint.take();
				} catch (InterruptedException e) {
				}
				if (returnmessage instanceof ReturnMessage) {
					ret = ((ReturnMessage) returnmessage).returnvalue;
				} else if (returnmessage instanceof ExceptionThrownMessage) {
					return ret;
				} else {
					System.err.println("Double Graft");
				}
			}
			return ret;
		} catch (NoGraftExecution nograft) {
			return nograft.value;
		} finally {
			while (!StackQueue.queue().isEmpty()) {
				PairOfContinueResultPoints pair = StackQueue.queue().poll();
				try {
					try {
						pair.continuePoint.put(new ContinueWithThrowMessage());
						pair.receivePoint.take();
					} catch (InterruptedException e) {
					}
				} catch (AlreadyClosed e) {
				}
			}
			StackQueue.pop();
		}
	}

	TA around(TAarg args) throws NoGraftExecution :advicePointPCD(args){
		ResultReceivePoint receivePoint = new ResultReceivePoint();
		Advice advice = new Advice(receivePoint) {
			public TC advice() throws RunWithoutExecution {// Adviceはここに展開
				TA pass=proceed(null);
				if(Main.isWeak(pass)){
					System.out.println("Too weak!! Enter more strong password.");
					return Main.chopPoint(null);
				}
				else return graft(pass);
			}
		};
		ContinuePoint continuePoint = advice.continuePoint;
		StackQueue.queue().add(
				new PairOfContinueResultPoints(continuePoint, receivePoint));
		advice.start();
		ResultMessage ret;
		try {
			ret = receivePoint.take();
			if (ret instanceof GraftCalledMessage) {
				return ((GraftCalledMessage) ret).graftedvalue;
			} else if (ret instanceof ReturnMessage) {
				throw new NoGraftExecution(((ReturnMessage) ret).returnvalue);
			}
		} catch (InterruptedException e) {// 無視
		}
		// unreachable
		return null;
	}
}
