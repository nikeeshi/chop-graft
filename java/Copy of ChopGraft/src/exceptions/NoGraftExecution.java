package exceptions;

import types.TC;

public class NoGraftExecution extends Error {
	private static final long serialVersionUID = 1L;
	public TC value;
	public NoGraftExecution(TC value){
		this.value=value;
	}
}
