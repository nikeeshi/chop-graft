package main;

import javax.swing.JOptionPane;

import types.*;

public class Main {
	public static int depth=0;
	public void begin(String s){
		for(int i=0;i<depth;i++)System.out.print("  ");
		System.out.println(s+" begin");
		depth++;
	}	
	public void end(String s){
		depth--;
		for(int i=0;i<depth;i++)System.out.print("  ");
		System.out.println(s+" end");
	}
	public static void log(String s){
		for(int i=0;i<depth;i++)System.out.print("  ");
		System.out.println(s+" log");
	}
	public static void main(String[] args) {
		new Main().run(new TCarg());
	}
	public void run(TCarg arg){
		begin("run");
		chopPoint(arg);
		end("run");
	}
	//setPassword
	public static TC chopPoint(TCarg x) {
		submit(advicePoint(null));
		return null;
	}
	public static void submit(TA ta){
		System.out.println("submit "+(String)ta.o);
	}
	//getUserInput
	public static TA advicePoint(TAarg x) {
		String input = JOptionPane.showInputDialog("Please input a value");
		return new TA(input);
	}
	public static boolean isWeak(TA pass) {
		return ((String)pass.o).length()<8;
	}
}
