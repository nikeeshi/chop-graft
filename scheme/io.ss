#lang racket
(require (planet dutchyn/aspectscheme:1:0/aspectscheme))
(require racket/control)

(define-syntax chop
  (syntax-rules ()
    [(_ PCD1 PCD2 ADVICE BODY ...)
     (let ([tag (make-continuation-prompt-tag 'choptag)]) 
       (fluid-around
        (&& PCD2
            (cflow PCD1))
        (lambda (proceed)
          (lambda ()
            (lambda args
              (shift-at tag graft (apply ((ADVICE graft proceed)) args)))))
        (fluid-around
         PCD1
         (lambda (proceed)
           (lambda ()
             (lambda args
               (reset-at tag (apply proceed args)))))
         BODY ...)))]))
(define-syntax chop-debug
  (syntax-rules ()
    [(_ TAG PCD1 PCD2 ADVICE BODY ...)
     (begin(printf "make tag ~a\n" TAG)
           (let ([tag (make-continuation-prompt-tag (string->symbol TAG))])
             (fluid-around
              (&& PCD2
                  (cflow PCD1))
              (lambda (proceed)
                (lambda ()
                  (lambda args
                    (begin(printf "shift start ~a\n" TAG)
                          (let [(ret (shift-at tag graft (apply ((ADVICE graft proceed)) args)))]
                            (printf "shift end   ~a\n" TAG)
                            ret)))))
              (fluid-around
               PCD1
               (lambda (proceed)
                 (lambda ()
                   (lambda args
                     (begin(printf "reset start ~a\n" TAG)
                           (let [(ret (reset-at tag (apply proceed args)))]
                             (printf "reset end   ~a\n" TAG)
                             ret)))))
               BODY ... ))))]))
;便利関数
(define case-number 0)
(define display-case-number
  (case-lambda 
    [()(begin
         (printf "\ncase~a\n" case-number)
         (set! case-number (add1 case-number)))]
    [(arg)(begin
            (printf "\ncase~a ~a\n" case-number arg)
            (set! case-number (add1 case-number)))]))
;入出力
(define variable1 0)
(define (func1)
  (func2))
(define (func2)
  (set! variable1 (add1 variable1))
  (func3 variable1))
(define (func3 x)
  x)
(define (reset-variable)
  (set! variable1 0))
;型
;以下Chop&Graftの例
(display-case-number)
(chop
 (call func1)
 (call func3)
 (lambda (graft proceed)
   (lambda()
     (lambda (x)
       (if (eqv? x 1)
           (func1)
           (graft (proceed x))))))
 (func1))

(reset-variable)
(display-case-number)
(chop
 (call func1)
 (call func3)
 (lambda (graft proceed)
   (lambda()
     (lambda (x)
       (if (x . < . 100)
           (func1)
           (graft (proceed x))))))
 (func1))

(reset-variable)
(display-case-number "こういう構文があったらなあという妄想")
(chop
 (call func1)
 (call func3)
 (lambda (graft proceed)
   (lambda()
     (lambda (x)
       (if (x . < . 100)
           (call-with-label func1 (1 . + . (call-with-noaspect func1 func1 x)))
           (graft (proceed x))))))
 (func1))